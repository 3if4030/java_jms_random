#
# Télécharger et dézipper ActiveMQ à partir de https://activemq.apache.org/components/classic/download/
# ACTIVEMQ_HOME est le répertoire d'installation

export ACTIVEMQ_HOME=~/apache-activemq-5.16.0

# Récupération des bibliothèque nécessaires
mkdir lib
cp $ACTIVEMQ_HOME/activemq-all-5.16.0.jar lib/
cp $ACTIVEMQ_HOME/lib/optional/jackson-core-2.9.10.jar lib/
cp $ACTIVEMQ_HOME/lib/optional/jackson-annotations-2.9.10.jar lib/
cp $ACTIVEMQ_HOME/lib/optional/jackson-databind-2.9.10.4.jar lib/

# Compilation
javac -cp ./lib/\* if4030/jms/*.java

# Lancement du broker
~/apache-activemq-5.16.0/bin/activemq start


# T2 
java -cp .:./lib/\* if4030.jms.RandomClient1
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer1

# T1 
java -cp .:./lib/\* if4030.jms.RandomServer1
# wait
# T2 
java -cp .:./lib/\* if4030.jms.RandomClient1

# RandomServer1 10 -> 20
# T2
java -cp .:./lib/\* if4030.jms.RandomClient1 1&; java -cp .:./lib/\* if4030.jms.RandomClient1 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer1

# RandomServer1 20 -> 10
# T2
java -cp .:./lib/\* if4030.jms.RandomClient1 1&; java -cp .:./lib/\* if4030.jms.RandomClient1 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer1 1&; java -cp .:./lib/\* if4030.jms.RandomServer1 2

# RandomServer1 200 -> 2000
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer1 1&; java -cp .:./lib/\* if4030.jms.RandomServer1 2
# Go to http://127.0.0.1:8161/admin/queues.jsp

##################################################

# T2 
java -cp .:./lib/\* if4030.jms.RandomClient2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2

# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2
# wait
# T2 
java -cp .:./lib/\* if4030.jms.RandomClient2

# T2
java -cp .:./lib/\* if4030.jms.RandomClient2 1&; java -cp .:./lib/\* if4030.jms.RandomClient2 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2

# RandomClient2 10 -> 20
# T2
java -cp .:./lib/\* if4030.jms.RandomClient2 1&; java -cp .:./lib/\* if4030.jms.RandomClient2 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2 1&; java -cp .:./lib/\* if4030.jms.RandomServer2 2

##################################################

# T2 
java -cp .:./lib/\* if4030.jms.RandomClient2Bis
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2

# T2
java -cp .:./lib/\* if4030.jms.RandomClient2Bis 1&; java -cp .:./lib/\* if4030.jms.RandomClient2Bis 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2

# RandomClient2Bis 10 -> 20
# T2
java -cp .:./lib/\* if4030.jms.RandomClient2Bis 1&; java -cp .:./lib/\* if4030.jms.RandomClient2Bis 2
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2 1&; java -cp .:./lib/\* if4030.jms.RandomServer2 2

##################################################

# T2 
java -cp .:./lib/\* if4030.jms.RandomClient2Ter
# T1 
java -cp .:./lib/\* if4030.jms.RandomServer2
# wait
# T2 
java -cp .:./lib/\* if4030.jms.RandomClient2Ter

# Arret du broker
~/apache-activemq-5.16.0/bin/activemq stop


