package if4030.jms;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class RandomClient1 {
    private String name;

    public RandomClient1( String name ) {
        this.name = name;
    }

    public void run() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
            Connection connection = connectionFactory.createConnection();
            connection.start();
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue destination = session.createQueue("IF4030.JMS");
            MessageConsumer consumer = session.createConsumer(destination);
            
            int i = 0;
            while( i < 10 ) {
                TextMessage message = (TextMessage) consumer.receive(1000);
                if (message != null) {
                    System.out.println("Consumer " + name + ": Received: " + message.getText());
                    ++i;
                }
                else {
                    System.out.println("Consumer " + name + ": No message received, waiting for another");
                }
                Thread.sleep((int) (400 * Math.random()));
            }
            consumer.close();
            session.close();
            connection.close();
        }
        catch(Exception e) {
            System.out.println("Consumer " + name + ": Caught: " + e);
            e.printStackTrace();
        }
    }
    
    public static void main( String[] args ) {
        String name = "0";
        if (args.length > 0) name = args[0];
        new RandomClient1(name).run();
    }
}
