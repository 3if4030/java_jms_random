package if4030.jms;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class RandomClient2Bis {
    private String name;

    public RandomClient2Bis( String name ) {
        this.name = name;
    }

    public void run() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
            Connection connection = connectionFactory.createConnection();
            connection.start();
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             Topic destination = session.createTopic("IF4030.JMS");
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(new RandomClientListener(name));
            Thread.sleep(5000);
            
            consumer.close();
            session.close();
            connection.close();
        }
        catch(Exception e) {
            System.out.println("Consumer " + name + ": Caught: " + e);
            e.printStackTrace();
        }
    }
    
    public static void main( String[] args ) {
        String name = "0";
        if (args.length > 0) name = args[0];
        new RandomClient2Bis(name).run();
    }
}
