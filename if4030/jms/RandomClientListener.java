package if4030.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class RandomClientListener implements MessageListener {

    private String name;
    public RandomClientListener(String name) {
        this.name = name;
    }

    @Override
    public void onMessage( Message message ) {
        try {
            System.out.println("Consumer " + name + ": Received: " + ((TextMessage)message).getText());
        }
        catch( JMSException e ) {
            System.out.println("Consumer " + name + ": Caught: " + e);
            e.printStackTrace();
        }
    }
}
