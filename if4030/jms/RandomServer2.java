package if4030.jms;


import javax.jms.Connection;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;

public class RandomServer2 {
    private String name;
    private int min;
    private int max;

    public RandomServer2(String name, int min, int max) {
        this.name = name;
        this.min = min;
        this.max = max;
    }
    
    public void run() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
            Connection connection = connectionFactory.createConnection();
            connection.start();
            
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            Queue destination = session.createQueue("IF4030.JMS");
            Topic destination = session.createTopic("IF4030.JMS");
            MessageProducer producer = session.createProducer(destination);
            
            for( int i = 0; i < 10; ++i ) {
                String message = "P" + name + ": " + nextRandom();
                System.out.println("Producer " + name + ": Sending: " + message);
                producer.send(session.createTextMessage(message));
                Thread.sleep((int) (400 * Math.random()));
            }
            producer.close();
            session.close();
            connection.close();
        }
        catch(Exception e) {
            System.out.println("Producer " + name + ": Caught: " + e);
            e.printStackTrace();
        }
    }
    
    public int nextRandom() {
        double alea = Math.random();
        return min + ( int ) (( max - min + 1 ) * alea );
    }

    public static void main( String[] args ) throws InterruptedException {
        String name = "0";
        if (args.length > 0) name = args[0];
        new RandomServer2(name, 1, 100).run();
    }
}
